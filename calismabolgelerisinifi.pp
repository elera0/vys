{-------------------------------------------------------------------------------

  Dosya: calismabolgelerisinifi.pp

  İşlev: çalışma bölge kayıtlarını yönetir

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit calismabolgelerisinifi;

interface

uses Classes, SysUtils, fgl, calismabolgesisinifi;

type
  TCalismaBolgeListesi = specialize TFPGObjectList<TCalismaBolgesi>;

  TCalismaBolgeleri = class
  private
    FCalismaBolgeListesi: TCalismaBolgeListesi;
    function CalismaBolgesiOku(SiraNo: Integer): TCalismaBolgesi;
    procedure CalismaBolgeAyarDosyasiniOku;
  public
    constructor Create(VerileriDosyadanYukle: Boolean = False);
    destructor Destroy; override;
    procedure Temizle;
    function ToplamCalismaBolgesi: Integer;
    property CalismaBolgesi[SiraNo: Integer]: TCalismaBolgesi read CalismaBolgesiOku;
  end;

implementation

uses LConvEncoding;

constructor TCalismaBolgeleri.Create(VerileriDosyadanYukle: Boolean = False);
begin

  FCalismaBolgeListesi := TCalismaBolgeListesi.Create(False);

  if(VerileriDosyadanYukle) then CalismaBolgeAyarDosyasiniOku;
end;

destructor TCalismaBolgeleri.Destroy;
begin

  Temizle;
  FreeAndNil(FCalismaBolgeListesi);
  inherited Destroy;
end;

procedure TCalismaBolgeleri.Temizle;
var
  i: Integer;
begin

  if(ToplamCalismaBolgesi > 0) then
  begin

    for i := ToplamCalismaBolgesi - 1 downto 0 do
    begin

      FCalismaBolgeListesi.Items[i].Destroy;
    end;
  end;
end;

function TCalismaBolgeleri.ToplamCalismaBolgesi: Integer;
begin

  Result := FCalismaBolgeListesi.Count;
end;

function TCalismaBolgeleri.CalismaBolgesiOku(SiraNo: Integer): TCalismaBolgesi;
begin

  if(SiraNo >= 0) and (SiraNo < ToplamCalismaBolgesi) then
    Result := FCalismaBolgeListesi.Items[SiraNo]
  else Result := nil;
end;

procedure TCalismaBolgeleri.CalismaBolgeAyarDosyasiniOku;
var
  SL: TStringList;
  i: Integer;
  sa: TStringArray;
  CB: TCalismaBolgesi;
begin

  SL := TStringList.Create;
  SL.LoadFromFile('calismabolgeleri.ini');

  Temizle;
  FCalismaBolgeListesi.Clear;

  for i := 0 to SL.Count - 1 do
  begin

    sa := SL[i].Split(';');
    CB := TCalismaBolgesi.Create;
    CB.Tanimlayici := sa[0].ToInteger;
    CB.Adi := sa[1];
    CB.Vardiyeler[1] := sa[2].ToInteger;
    CB.Vardiyeler[2] := sa[3].ToInteger;
    CB.Vardiyeler[3] := sa[4].ToInteger;
    CB.Vardiyeler[4] := sa[5].ToInteger;
    CB.Vardiyeler[5] := sa[6].ToInteger;
    CB.Vardiyeler[6] := sa[7].ToInteger;
    CB.Vardiyeler[7] := sa[8].ToInteger;
    FCalismaBolgeListesi.Add(CB);
  end;

  FreeAndNil(SL);
end;

end.
