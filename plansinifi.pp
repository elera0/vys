{-------------------------------------------------------------------------------

  Dosya: plansinifi.pp

  İşlev: ay bazlı günlük iş planları yönetir

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit plansinifi;

interface

uses Classes, SysUtils, fgl, calismagunsinifi, calismabolgelerisinifi,
  vardiyelersinifi, personellersinifi;

type
  TCalismaGunleri = specialize TFPGObjectList<TCalismaGunu>;

  TPlanlar = class
  private
    FMevcutVardiyeler: TVardiyeler;
    FMevcutCalismaBolgeleri: TCalismaBolgeleri;
    FMevcutPersoneller: TPersoneller;
    FPlanlananCalismaGunleri: TCalismaGunleri;
    function PlanlananCalismaGunuOku(SiraNo: Integer): TCalismaGunu;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Temizle;
    function ToplamPlanlananCalismaGunu: Integer;
    property PlanlananCalismaGunu[SiraNo: Integer]: TCalismaGunu read PlanlananCalismaGunuOku;

    property MevcutVardiyeler: TVardiyeler read FMevcutVardiyeler;
    property MevcutCalismaBolgeleri: TCalismaBolgeleri read FMevcutCalismaBolgeleri;
    property MevcutPersoneller: TPersoneller read FMevcutPersoneller;
  end;

implementation

uses dateutils, genel, calismabolgesisinifi;

constructor TPlanlar.Create;
var
  i, j, k: Integer;
  N, N2: TDateTime;
  SonGun, Ay, HaftaninGunu: Word;
  CG: TCalismaGunu;
  CB, CB2: TCalismaBolgesi;
begin

  FMevcutVardiyeler := TVardiyeler.Create;
  FMevcutCalismaBolgeleri := TCalismaBolgeleri.Create(True);
  FMevcutPersoneller := TPersoneller.Create;

  FPlanlananCalismaGunleri := TCalismaGunleri.Create(False);

  N := Now;
  Ay := MonthOf(N);

  N2 := EndOfTheMonth(N);
  SonGun := DayOf(N2);

  N2 := RecodeDay(N, 1);
  for i := 1 to SonGun do
  begin

    HaftaninGunu := DayOfTheWeek(N2);

    CG := TCalismaGunu.Create;
    CG.Gun := i;
    CG.Ay := Ay;
    CG.HaftaninGunu := HaftaninGunu;
    FPlanlananCalismaGunleri.Add(CG);

    if(MevcutCalismaBolgeleri.ToplamCalismaBolgesi > 0) then
    begin

      for j := 0 to MevcutCalismaBolgeleri.ToplamCalismaBolgesi - 1 do
      begin

        CB := MevcutCalismaBolgeleri.CalismaBolgesi[j];

        CB2 := CG.CalismaBolgesiEkle;
        CB2.Adi := CB.Adi;
        for k := 1 to 7 do CB2.Vardiyeler[k] := CB.Vardiyeler[k];
        CB2.MevcutVardiyeGunu := HaftaninGunu;
      end;
    end;

    N2 := N2 + 1;
  end;
end;

destructor TPlanlar.Destroy;
begin

  FreeAndNil(FMevcutPersoneller);
  FreeAndNil(FMevcutCalismaBolgeleri);
  FreeAndNil(FMevcutVardiyeler);

  Temizle;
  FreeAndNil(FPlanlananCalismaGunleri);
  inherited Destroy;
end;

function TPlanlar.ToplamPlanlananCalismaGunu: Integer;
begin

  Result := FPlanlananCalismaGunleri.Count;
end;

procedure TPlanlar.Temizle;
var
  i: Integer;
begin

  if(ToplamPlanlananCalismaGunu > 0) then
  begin

    for i := ToplamPlanlananCalismaGunu - 1 downto 0 do
    begin

      FPlanlananCalismaGunleri.Items[i].Destroy;
    end;
  end;
end;

function TPlanlar.PlanlananCalismaGunuOku(SiraNo: Integer): TCalismaGunu;
begin

  if(SiraNo >= 0) and (SiraNo < ToplamPlanlananCalismaGunu) then
    Result := FPlanlananCalismaGunleri.Items[SiraNo]
  else Result := nil;
end;

end.
