{-------------------------------------------------------------------------------

  Dosya: vardiyesinifi.pp

  İşlev: vardiye bilgi kayıtlarını saklar

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit vardiyesinifi;

interface

uses Classes, SysUtils;

type
  TVardiye = class
  private
    FTanimlayici: Integer;
    FVardiyeAdi: string;
    FIsBaslamaSaati, FIsBitisSaati: string;
  public
  published
    property Tanimlayici: Integer read FTanimlayici write FTanimlayici;
    property VardiyeAdi: string read FVardiyeAdi write FVardiyeAdi;
    property IsBaslamaSaati: string read FIsBaslamaSaati write FIsBaslamaSaati;
    property IsBitisSaati: string read FIsBitisSaati write FIsBitisSaati;
  end;

implementation

end.
