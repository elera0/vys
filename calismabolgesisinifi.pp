{-------------------------------------------------------------------------------

  Dosya: calismabolgesisinifi.pp

  İşlev: çalışma bölge kaydını saklar

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit calismabolgesisinifi;

interface

uses Classes, SysUtils;

type
  TCalismaBolgesi = class
  private
    FTanimlayici: Integer;
    FAdi: string;                           // çalışma bölgesi adı
    FVardiyeler: array[1..7] of Integer;    // 1 = pazartesi ...
    FMevcutVardiyeGunu: Integer;
    function VardiyeOku(SiraNo: Integer): Integer;
    procedure VardiyeYaz(SiraNo: Integer; ADeger: Integer);
  public
    constructor Create;
    destructor Destroy; override;
    property Vardiyeler[SiraNo: Integer]: Integer read VardiyeOku write VardiyeYaz;
  published
    property Tanimlayici: Integer read FTanimlayici write FTanimlayici;
    property Adi: string read FAdi write FAdi;
    property MevcutVardiyeGunu: Integer read FMevcutVardiyeGunu write FMevcutVardiyeGunu;
  end;

implementation

constructor TCalismaBolgesi.Create;
var
  i: Integer;
begin

  for i := 1 to 7 do FVardiyeler[i] := 0;

  FMevcutVardiyeGunu := 1;    // pazartesi günü
end;

destructor TCalismaBolgesi.Destroy;
begin

  inherited Destroy;
end;

function TCalismaBolgesi.VardiyeOku(SiraNo: Integer): Integer;
begin

  if(SiraNo >= 1) and (SiraNo <= 7) then Result := FVardiyeler[SiraNo];
end;

procedure TCalismaBolgesi.VardiyeYaz(SiraNo: Integer; ADeger: Integer);
begin

  if(SiraNo >= 1) and (SiraNo <= 7) then FVardiyeler[SiraNo] := ADeger;
end;

end.
