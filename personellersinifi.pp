{-------------------------------------------------------------------------------

  Dosya: personellersinifi.pp

  İşlev: personel kayıt verilerini yönetir

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit personellersinifi;

interface

uses Classes, SysUtils, fgl, personelsinifi;

type
  TPersonelListesi = specialize TFPGObjectList<TPersonel>;

  TPersoneller = class
  private
    FPersonelListesi: TPersonelListesi;
    function PersonelOku(SiraNo: Integer): TPersonel;
    procedure PersonelAyarDosyasiniOku;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Temizle;
    function ToplamPersonel: Integer;
    property Personel[SiraNo: Integer]: TPersonel read PersonelOku;
  end;

implementation

uses LConvEncoding;

constructor TPersoneller.Create;
begin

  FPersonelListesi := TPersonelListesi.Create(False);

  PersonelAyarDosyasiniOku;
end;

destructor TPersoneller.Destroy;
begin

  Temizle;
  FreeAndNil(FPersonelListesi);
  inherited Destroy;
end;

procedure TPersoneller.Temizle;
var
  i: Integer;
begin

  if(ToplamPersonel > 0) then
  begin

    for i := ToplamPersonel - 1 downto 0 do
    begin

      FPersonelListesi.Items[i].Destroy;
    end;
  end;
end;

function TPersoneller.ToplamPersonel: Integer;
begin

  Result := FPersonelListesi.Count;
end;

function TPersoneller.PersonelOku(SiraNo: Integer): TPersonel;
begin

  if(SiraNo >= 0) and (SiraNo < ToplamPersonel) then
    Result := FPersonelListesi.Items[SiraNo]
  else Result := nil;
end;

procedure TPersoneller.PersonelAyarDosyasiniOku;
var
  SL: TStringList;
  i: Integer;
  sa: TStringArray;
  P: TPersonel;
begin

  SL := TStringList.Create;
  SL.LoadFromFile('personellistesi.ini');

  Temizle;
  FPersonelListesi.Clear;

  for i := 0 to SL.Count - 1 do
  begin

    sa := SL[i].Split(';');
    P := TPersonel.Create;
    P.Tanimlayici := sa[0].ToInteger;
    P.AdSoyad := sa[1];
    P.CalisilacakVardiyeler := sa[2].ToInteger;
    P.IlkVardiye := sa[3].ToInteger;
    P.TatilGunu := sa[4].ToInteger;
    FPersonelListesi.Add(P);
  end;

  FreeAndNil(SL);
end;

end.
