{-------------------------------------------------------------------------------

  Dosya: anapencere.pp

  İşlev: program ana penceresi

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit anapencere;

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  Grids, ExtCtrls, calismabolgesisinifi, calismagunsinifi, personelsinifi, Types;

type
  TfrmAnaPencere = class(TForm)
    btnGorevleriListele: TButton;
    lblBilgi: TLabel;
    lvGorevYerleri: TListView;
    lvPersonelListesi: TListView;
    lvVardiyeListesi: TListView;
    pcAnaSayfa: TPageControl;
    pnlGorevleriListele: TPanel;
    pnlBilgi: TPanel;
    sgPersonelVardiyeListesi: TStringGrid;
    sbDurumCubugu: TStatusBar;
    tsGorevYerleri: TTabSheet;
    tsPersonelListesi: TTabSheet;
    tsPersonelVardiyeListesi: TTabSheet;
    tsVardiyeListesi: TTabSheet;
    procedure FormDestroy(Sender: TObject);
    procedure MevcutCalismaBolgeleriniListele;
    procedure MevcutPersonelleriListele;
    procedure btnGorevleriListeleClick(Sender: TObject);
    procedure MevcutVardiyeleriListele;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgPersonelVardiyeListesiDrawCell(Sender: TObject; aCol,
      aRow: Integer; aRect: TRect; aState: TGridDrawState);
  private

  public

  end;

var
  frmAnaPencere: TfrmAnaPencere;

implementation

{$R *.lfm}

uses genel, plansinifi, vardiyelersinifi, dateutils, calismabolgelerisinifi,
  personellersinifi, vardiyesinifi;

procedure TfrmAnaPencere.FormCreate(Sender: TObject);
begin

  GPlanlar := TPlanlar.Create;
end;

procedure TfrmAnaPencere.FormShow(Sender: TObject);
begin

  MevcutVardiyeleriListele;
  MevcutCalismaBolgeleriniListele;
  MevcutPersonelleriListele;
end;

procedure TfrmAnaPencere.FormDestroy(Sender: TObject);
begin

  FreeAndNil(GPlanlar);
end;

procedure TfrmAnaPencere.MevcutVardiyeleriListele;
var
  li: TListItem;
  i: Integer;
  V: TVardiye;
begin

  if(GPlanlar.MevcutVardiyeler.ToplamVardiye > 0) then
  begin

    for i := 0 to GPlanlar.MevcutVardiyeler.ToplamVardiye - 1 do
    begin

      V := GPlanlar.MevcutVardiyeler.Vardiye[i];
      li := lvVardiyeListesi.Items.Add;
      li.Caption := (i + 1).ToString;
      li.SubItems.Add(V.Tanimlayici.ToString);
      li.SubItems.Add(V.VardiyeAdi);
      li.SubItems.Add(V.IsBaslamaSaati);
      li.SubItems.Add(V.IsBitisSaati);
    end;
  end;
end;

procedure TfrmAnaPencere.MevcutCalismaBolgeleriniListele;
var
  li: TListItem;
  i: Integer;
  CalismaBolgesi: TCalismaBolgesi;
begin

  if(GPlanlar.MevcutCalismaBolgeleri.ToplamCalismaBolgesi > 0) then
  begin

    for i := 0 to GPlanlar.MevcutCalismaBolgeleri.ToplamCalismaBolgesi - 1 do
    begin

      CalismaBolgesi := GPlanlar.MevcutCalismaBolgeleri.CalismaBolgesi[i];
      li := lvGorevYerleri.Items.Add;
      li.Caption := (i + 1).ToString;
      li.SubItems.Add(CalismaBolgesi.Tanimlayici.ToString);
      li.SubItems.Add(CalismaBolgesi.Adi);
      li.SubItems.Add(CalismaBolgesi.Vardiyeler[1].ToString);
      li.SubItems.Add(CalismaBolgesi.Vardiyeler[2].ToString);
      li.SubItems.Add(CalismaBolgesi.Vardiyeler[3].ToString);
      li.SubItems.Add(CalismaBolgesi.Vardiyeler[4].ToString);
      li.SubItems.Add(CalismaBolgesi.Vardiyeler[5].ToString);
      li.SubItems.Add(CalismaBolgesi.Vardiyeler[6].ToString);
      li.SubItems.Add(CalismaBolgesi.Vardiyeler[7].ToString);
    end;
  end;
end;

procedure TfrmAnaPencere.MevcutPersonelleriListele;
var
  li: TListItem;
  i: Integer;
  P: TPersonel;
begin

  if(GPlanlar.MevcutPersoneller.ToplamPersonel > 0) then
  begin

    for i := 0 to GPlanlar.MevcutPersoneller.ToplamPersonel - 1 do
    begin

      P := GPlanlar.MevcutPersoneller.Personel[i];

      li := lvPersonelListesi.Items.Add;
      li.Caption := (i + 1).ToString;
      li.SubItems.Add(P.Tanimlayici.ToString);
      li.SubItems.Add(P.AdSoyad);
      li.SubItems.Add(P.CalisilacakVardiyeler.ToString);
      li.SubItems.Add(P.IlkVardiye.ToString);
      li.SubItems.Add(P.TatilGunu.ToString);
    end;
  end;
end;

procedure TfrmAnaPencere.btnGorevleriListeleClick(Sender: TObject);
var
  i, j: Integer;
  CB: TCalismaBolgesi;
  CG: TCalismaGunu;
begin

  sgPersonelVardiyeListesi.ColWidths[0] := 120;
  sgPersonelVardiyeListesi.RowHeights[0] := 50;

  // ayın günleri
  sgPersonelVardiyeListesi.ColCount := GPlanlar.ToplamPlanlananCalismaGunu + 1;
  for i := 0 to GPlanlar.ToplamPlanlananCalismaGunu - 1 do
  begin

    CG := GPlanlar.PlanlananCalismaGunu[i];
    sgPersonelVardiyeListesi.Cells[i + 1, 0] := CG.Gun.ToString + ' ' +
      Aylar[CG.Ay] + ' - ' + Gunler[CG.HaftaninGunu];
    sgPersonelVardiyeListesi.ColWidths[i + 1] := 120;
  end;

  // çalışma yerleri
  sgPersonelVardiyeListesi.RowCount := GPlanlar.PlanlananCalismaGunu[0].ToplamCalismaBolgesi + 1;
  if(GPlanlar.PlanlananCalismaGunu[0].ToplamCalismaBolgesi > 0) then
  begin

    for i := 0 to GPlanlar.PlanlananCalismaGunu[0].ToplamCalismaBolgesi - 1 do
    begin

      CB := GPlanlar.PlanlananCalismaGunu[0].CalismaBolgesi[i];
      sgPersonelVardiyeListesi.Cells[0, i + 1] := CB.Adi;
      sgPersonelVardiyeListesi.RowHeights[i + 1] := 50;
    end;

    for i := 0 to GPlanlar.ToplamPlanlananCalismaGunu - 1 do
    begin

      for j := 0 to GPlanlar.PlanlananCalismaGunu[i].ToplamCalismaBolgesi - 1 do
      begin

        CB := GPlanlar.PlanlananCalismaGunu[i].CalismaBolgesi[j];
        sgPersonelVardiyeListesi.Cells[i + 1, j + 1] := IntToStr(CB.Vardiyeler[CB.MevcutVardiyeGunu]);
      end;
    end;
  end;
end;

procedure TfrmAnaPencere.sgPersonelVardiyeListesiDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
begin

  if(aCol = 0) or (aRow = 0) then
  begin

    sgPersonelVardiyeListesi.Canvas.Brush.Color := cl3DLight;
    sgPersonelVardiyeListesi.Canvas.Pen.Color := clBlack;
  end
  else
  begin

    sgPersonelVardiyeListesi.Canvas.Brush.Color := clWhite;
    sgPersonelVardiyeListesi.Canvas.Pen.Color := clNavy;
  end;
  sgPersonelVardiyeListesi.Canvas.FillRect(aRect);

  sgYaziOrtala(sgPersonelVardiyeListesi, aRect, sgPersonelVardiyeListesi.Cells[aCol, aRow]);
end;

end.
