{-------------------------------------------------------------------------------

  Dosya: personelsinifi.pp

  İşlev: personel bilgi kayıtlarını saklar

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit personelsinifi;

interface

uses Classes, SysUtils;

type
  TPersonel = class
  private
    FTanimlayici: Integer;
    FAdSoyad: string;
    FCalisilacakVardiyeler,
    FIlkVardiye, FTatilGunu: Integer;
  published
    property Tanimlayici: Integer read FTanimlayici write FTanimlayici;
    property AdSoyad: string read FAdSoyad write FAdSoyad;
    property CalisilacakVardiyeler: Integer read FCalisilacakVardiyeler write FCalisilacakVardiyeler;
    property IlkVardiye: Integer read FIlkVardiye write FIlkVardiye;
    property TatilGunu: Integer read FTatilGunu write FTatilGunu;
  end;

implementation

end.
