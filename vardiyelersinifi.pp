{-------------------------------------------------------------------------------

  Dosya: vardiyelersinifi.pp

  İşlev: vardiye tanım kayıtlarını yönetir

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit vardiyelersinifi;

interface

uses Classes, SysUtils, fgl, vardiyesinifi;

type
  TVardiyeListesi = specialize TFPGObjectList<TVardiye>;

  TVardiyeler = class
  private
    FVardiyeListesi: TVardiyeListesi;
    function VardiyeOku(SiraNo: Integer): TVardiye;
    procedure VardiyeAyarDosyasiniOku;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Temizle;
    function ToplamVardiye: Integer;
    property Vardiye[Index: Integer]: TVardiye read VardiyeOku;
  end;

implementation

uses LConvEncoding;

constructor TVardiyeler.Create;
begin

  FVardiyeListesi := TVardiyeListesi.Create(False);

  VardiyeAyarDosyasiniOku;
end;

destructor TVardiyeler.Destroy;
begin

  Temizle;
  FreeAndNil(FVardiyeListesi);
  inherited Destroy;
end;

procedure TVardiyeler.Temizle;
var
  i: Integer;
begin

  if(ToplamVardiye > 0) then
  begin

    for i := ToplamVardiye - 1 downto 0 do
    begin

      FVardiyeListesi.Items[i].Destroy;
    end;
  end;
end;

function TVardiyeler.ToplamVardiye: Integer;
begin

  Result := FVardiyeListesi.Count;
end;

function TVardiyeler.VardiyeOku(SiraNo: Integer): TVardiye;
begin

  if(SiraNo >= 0) and (SiraNo < ToplamVardiye) then
    Result := FVardiyeListesi.Items[SiraNo]
  else Result := nil;
end;

procedure TVardiyeler.VardiyeAyarDosyasiniOku;
var
  SL: TStringList;
  i: Integer;
  sa: TStringArray;
  V: TVardiye;
begin

  SL := TStringList.Create;
  SL.LoadFromFile('vardiyeler.ini');

  Temizle;
  FVardiyeListesi.Clear;

  for i := 0 to SL.Count - 1 do
  begin

    sa := SL[i].Split(';');
    V := TVardiye.Create;
    V.Tanimlayici := sa[0].ToInteger;
    V.VardiyeAdi := sa[1];
    V.IsBaslamaSaati := sa[2];
    V.IsBitisSaati := sa[3];
    FVardiyeListesi.Add(V);
  end;

  FreeAndNil(SL);
end;

end.
