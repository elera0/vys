{-------------------------------------------------------------------------------

  Dosya: vys.lpr

  İşlev: vardiye yönetim sistemi - proje dosyası

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
program vys;

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, anapencere, vardiyelersinifi, calismabolgelerisinifi, personellersinifi, plansinifi,
  calismagunsinifi, calismabolgesisinifi, vardiyesinifi, personelsinifi
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Title:='Vardiye Yönetim Sistemi';
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TfrmAnaPencere, frmAnaPencere);
  Application.Run;
end.

