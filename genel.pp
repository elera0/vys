{-------------------------------------------------------------------------------

  Dosya: genel.pp

  İşlev: genel tanım / değişken / sınıf / işlev dosyası

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit genel;

interface

uses Classes, SysUtils, StdCtrls, Grids, plansinifi;

var
  GPlanlar: TPlanlar;

const
  Aylar: array[0..12] of string = ('?', 'Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs',
    'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık');

  Gunler: array[0..7] of string = ('?', 'Pt', 'Sa', 'Ça', 'Pe', 'Cu', 'Ct', 'Pa');

procedure sgYaziOrtala(sg: TStringGrid; ARect: TRect; s: string);
procedure sgYaziSolaYasla(sg: TStringGrid; ARect: TRect; s: string);

implementation

// StringGrid nesnesinin kolonuna yatay / dikey ortalı değer yazar
procedure sgYaziOrtala(sg: TStringGrid; ARect: TRect; s: string);
var
  dx, dy, x, y: Integer;
begin

  dx := sg.Canvas.TextWidth(s);
  dy := sg.Canvas.TextHeight(s);

  if(dx > aRect.Width) then
    x := ARect.Left
  else x := ((aRect.Width div 2) - (dx div 2)) + aRect.Left;

  if(dy > aRect.Height) then
    y := ARect.Top
  else y := ((aRect.Height div 2) - (dy div 2)) + aRect.Top;

  sg.Canvas.TextOut(x, y, s);
end;

// StringGrid nesnesinin kolonuna sola dayalı / dikey ortalı değer yazar
procedure sgYaziSolaYasla(sg: TStringGrid; ARect: TRect; s: string);
var
  dx, dy, x, y: Integer;
begin

  dx := sg.Canvas.TextWidth(s);
  dy := sg.Canvas.TextHeight(s);

  x := 2;

  if(dy > aRect.Height) then
    y := ARect.Top
  else y := ((aRect.Height div 2) - (dy div 2)) + aRect.Top;

  sg.Canvas.TextOut(x, y, s);
end;

end.
