{-------------------------------------------------------------------------------

  Dosya: calismagunsinifi.pp

  İşlev: çalışma günlerini planlama / yönetme ana sınıfı

  Güncelleme Tarihi: 04/08/2019

-------------------------------------------------------------------------------}
{$mode objfpc}{$H+}
unit calismagunsinifi;

interface

uses Classes, SysUtils, fgl, calismabolgesisinifi;

type
  TCalismaBolgeListesi = specialize TFPGObjectList<TCalismaBolgesi>;

  TCalismaGunu = class
  private
    FGun, FAy, FHaftaninGunu: Integer;
    FCalismaBolgeListesi: TCalismaBolgeListesi;
    function CalismaBolgesiOku(SiraNo: Integer): TCalismaBolgesi;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function ToplamCalismaBolgesi: Integer;
    property CalismaBolgesi[SiraNo: Integer]: TCalismaBolgesi read CalismaBolgesiOku;

    function CalismaBolgesiEkle: TCalismaBolgesi;

    property Gun: Integer read FGun write FGun;
    property Ay: Integer read FAy write FAy;
    property HaftaninGunu: Integer read FHaftaninGunu write FHaftaninGunu;
  end;

implementation

constructor TCalismaGunu.Create;
begin

  FCalismaBolgeListesi := TCalismaBolgeListesi.Create(False);
end;

destructor TCalismaGunu.Destroy;
begin

  Clear;
  FreeAndNil(FCalismaBolgeListesi);
  inherited Destroy;
end;

procedure TCalismaGunu.Clear;
var
  i: Integer;
begin

  if(ToplamCalismaBolgesi > 0) then
  begin

    for i := ToplamCalismaBolgesi - 1 downto 0 do
    begin

      FCalismaBolgeListesi.Items[i].Destroy;
    end;
  end;
end;

function TCalismaGunu.ToplamCalismaBolgesi: Integer;
begin

  Result := FCalismaBolgeListesi.Count;
end;

function TCalismaGunu.CalismaBolgesiOku(SiraNo: Integer): TCalismaBolgesi;
begin

  if(SiraNo >= 0) and (SiraNo < ToplamCalismaBolgesi) then
    Result := FCalismaBolgeListesi.Items[SiraNo]
  else Result := nil;
end;

function TCalismaGunu.CalismaBolgesiEkle: TCalismaBolgesi;
begin

  Result := TCalismaBolgesi.Create;
  FCalismaBolgeListesi.Add(Result);
end;

end.
